```plantuml
@startuml
left to right direction
actor 某某某 as human
(聪明) as (feature1)
(勤奋) as (feature2)
(多才) as (feature3)
(多艺) as (feature4)

human .> feature1:include
human .> feature2:include
human .> feature3:include
human .> feature4:include


@enduml
```